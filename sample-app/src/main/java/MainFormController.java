import java.io.IOException;
import java.util.Optional;

import com.danieldc.minifx.FormController;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainFormController extends FormController<MainFormModel, BorderPane> {
    @FXML
    void onHandleFileClose() {
        System.err.println( "MainFormController: onHandleFileClose" );
    }

    @FXML
    void onHandleTestDialog() throws IOException {
        System.err.println( "MainFormController: onHandleTestDialog" );
        Optional<ButtonType> retVal = LoginDialogController.showModal( getStage(), new LoginDialogModel( "user1", "pwd1", "server1" ) );
        System.err.println( "MainFormController: LoginDialogController exit with " + retVal.toString() );
    }


    @Override
    protected void onInitBind( Stage stage, Scene scene, BorderPane pane, MainFormModel model ) {
        // Nothing to do. The scene, pane and model are already saved in the base class
    }

    public static Scene newScene( Stage stage, MainFormModel model ) throws IOException {
        return FormController.<MainFormController, MainFormModel, BorderPane>newScene( "MainForm.fxml", stage, model );
    }
}
