import java.io.IOException;
import java.util.Optional;

import com.danieldc.minifx.DialogController;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginDialogController extends DialogController<LoginDialogController, LoginDialogModel, Parent> {
    @FXML
    TextField _userName;
    @FXML
    TextField _userPassword;
    @FXML
    TextField _server;

    private Optional<ButtonType> _retVal = Optional.empty();

    public static Optional<ButtonType> showModal( Stage parentStage, LoginDialogModel model ) throws IOException {
        return DialogController.showModal( "LoginDialog.fxml", parentStage, "Please login", model );
    }

    @FXML
    private void initialize() {
        System.err.println( "LoginDialogController: FXML-initialize" );
    }

    @FXML
    private void handleOk() {
        System.err.println( "LoginDialogController: handleOk" );
        _retVal = Optional.of( ButtonType.OK );
        getStage().close();
    }

    @FXML
    private void handleCancel() {
        System.err.println( "LoginDialogController: handleCancel" );
        _retVal = Optional.of( ButtonType.CANCEL );
        getStage().close();
    }

    @Override
    protected Optional<ButtonType> getRetVal() {
        return _retVal;
    }

    @Override
    protected void onInitBind( Stage stage, Scene scene, LoginDialogModel model ) {
        _userName.textProperty().bindBidirectional( model.userNameProperty() );
        _userPassword.textProperty().bindBidirectional( model.userPasswordProperty() );
        _server.textProperty().bind( model.serverProperty() );  // This field could not be changed via UI (.bind vs .bindBidirectional)
    }
}
