import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {
    @Override
    public void start( Stage primaryStage ) throws Exception {
        Scene scene = MainFormController.newScene( primaryStage, new MainFormModel() );
        primaryStage.setScene( scene );
        primaryStage.show();
    }

    public static void main( String[] args ) {
        Application.launch( App.class, args );
    }
}
