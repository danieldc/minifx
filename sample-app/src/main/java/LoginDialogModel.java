import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LoginDialogModel {
    private StringProperty _userName = new SimpleStringProperty( this, "userName", null );
    private StringProperty _userPassword = new SimpleStringProperty( this, "userPassword", null );
    private StringProperty _server = new SimpleStringProperty( this, "server", null );

    public LoginDialogModel() {
        this( null, null, null );
    }

    public LoginDialogModel( String defUserName, String defUserPassword, String defServer ) {
        _userName.set( defUserName );
        _userPassword.set( defUserPassword );
        _server.set( defServer );
    }

    public String getUserName() {
        return _userName.get();
    }

    public StringProperty userNameProperty() {
        return _userName;
    }

    public void setUserName( String _userName ) {
        this._userName.set( _userName );
    }

    public String getUserPassword() {
        return _userPassword.get();
    }

    public StringProperty userPasswordProperty() {
        return _userPassword;
    }

    public void setUserPassword( String _userPassword ) {
        this._userPassword.set( _userPassword );
    }

    public String getServer() {
        return _server.get();
    }

    public StringProperty serverProperty() {
        return _server;
    }

    public void setServer( String _server ) {
        this._server.set( _server );
    }
}
