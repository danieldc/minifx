package com.danieldc.minifx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public abstract class DialogController<TController extends DialogController, TModel, TPane extends Parent> {
    private Stage _stage;
    private Scene _scene;
    private TModel _model;

    protected abstract void onInitBind( Stage stage, Scene scene, TModel model );
    protected abstract Optional<ButtonType> getRetVal();

    public Stage getStage() {
        return _stage;
    }
    public Scene getScene() {
        return _scene;
    }
    public TModel getModel() {
        return _model;
    }

    public static <TController extends DialogController, TModel, TPane extends Parent> Optional<ButtonType> showModal( String fxmlFilePath, Stage parentStage, String stageTitle, TModel model ) throws IOException {
        FXMLLoader loader = new FXMLLoader( model.getClass().getResource( fxmlFilePath ) );
        TPane pane = loader.load();

        Scene scene = new Scene( pane );

        Stage stage = new Stage();
        stage.setTitle( stageTitle );
        stage.initModality( Modality.WINDOW_MODAL );
        stage.initOwner( parentStage );
        stage.setScene( scene );

        TController controller = loader.getController();
        ((DialogController) controller)._stage = stage;
        ((DialogController) controller)._scene = scene;
        ((DialogController) controller)._model = model;
        ((DialogController) controller).onInitBind( stage, scene, model );
        stage.showAndWait();
        return controller.getRetVal();
    }

}
