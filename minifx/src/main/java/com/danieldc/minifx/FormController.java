package com.danieldc.minifx;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public abstract class FormController<TModel, TPane extends Parent> {
    private Stage _stage;
    private Scene _scene;
    private TPane _pane;
    private TModel _model;

    public Stage getStage() {
        return _stage;
    }
    public Scene getScene() {
        return _scene;
    }
    public TPane getPane() {
        return _pane;
    }
    public TModel getModel() {
        return _model;
    }

    protected abstract void onInitBind( Stage stage, Scene scene, TPane pane, TModel model );


    public static <TController extends FormController, TModel, TPane extends Parent> Scene newScene( String fxmlFilePath, Stage stage, TModel model ) throws IOException {
        URL resource = model.getClass().getResource( fxmlFilePath );
        // Object c = TController.class;
        FXMLLoader loader = new FXMLLoader( resource );
        TPane pane = loader.load();
        TController controller = loader.getController();
        Scene scene = new Scene( pane );
        controller.onInitBind( stage, scene, pane, model );
        return scene;
    }

}
